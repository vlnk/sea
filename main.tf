# [[file:Website.org::*avec terraform][avec terraform:1]]
terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

locals {
  assets_dir = "assets/"
}

provider "digitalocean" {}

resource "digitalocean_spaces_bucket" "assets" {
  name   = "numeriphare-website-assets"
  region = "sfo3"
  acl = "public-read"
}

resource "digitalocean_spaces_bucket_policy" "assets" {
  region = digitalocean_spaces_bucket.assets.region
  bucket = digitalocean_spaces_bucket.assets.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Sid = "PublicReadGetObject"
      Effect = "Allow"
      Principal = "*"
      Action  = "s3:GetObject"
      Resource = "${digitalocean_spaces_bucket.assets.name}/*"
    }]
  })
}

resource "digitalocean_spaces_bucket_object" "index" {
  region = digitalocean_spaces_bucket.assets.region
  bucket = digitalocean_spaces_bucket.assets.name

  for_each = fileset(local.assets_dir, "**/*")
  key = each.value
  source = "${local.assets_dir}/${each.value}"
  etag = filemd5("${local.assets_dir}/${each.value}")
}
# avec terraform:1 ends here
