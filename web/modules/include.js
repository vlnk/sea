/**
 * injects HTML into an element from a file
 * @param {HTMLElement} $el - the element to inject
 * @param {string} filePath - the path of the file
 */
async function includeSource($el, filePath) {
  try {
    const response = await fetch(filePath)

    if (response.ok) {
      $el.innerHTML = await response.text()
      $el.removeAttribute('src')
    }

    $el.parentNode.replaceChild($el, $el)
    console.debug(`included html from ${filePath}`)

  } catch (err) {
    console.error(err.message)
  }

  return $el
}

/**
 * reloads an injected script (in order to execute them)
 * @param {HTMLElement} $script - a script element
 */
function reloadScript ($script) {
  const $reloaded = document.createElement('script')

  Array.from($script.attributes)
    .forEach(attr => $reloaded.setAttribute(attr.name, attr.value))

  $reloaded.appendChild(document.createTextNode($script.innerHTML))
  $script.parentNode.replaceChild($reloaded, $script)
}

/**
 * reloads all injected scripts of an element
 * @param {HTMLElement} $el - an injected element
 */
function reloadScripts ($el) {
  $el.querySelectorAll('script')
    .forEach($script => reloadScript($script))
}

/** injects all element with the attribute source */
function includeNested ($root) {
  $root.querySelectorAll('[data-src]')
    .forEach(async $el => {
      const srcPath = $el.getAttribute('data-src')
      const $injected = await includeSource($el, srcPath)

      reloadScripts($injected)
      includeNested($injected)
    })
}

includeNested(document)
