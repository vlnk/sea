const supportedThemes = new Set([
  'dark',
  'light'
])

/**
 * gets the current theme to use
 * @returns {string} the current theme
 */
function getCurrentTheme () {
  return localStorage.getItem('theme') || 'dark';
}

/**
 * set the selected theme in the localStorage and switch to the theme
 * @param {string} newTheme - the selected theme
 */
function setLocalTheme (newTheme) {
  if (newTheme === getCurrentTheme()) return

  console.debug(`set new local theme ${newTheme}`)
  localStorage.setItem('theme', newTheme)
  switchTheme(getCurrentTheme())
}

/**
 * updates the theme selector by binding `onclick` methods
 * @param {string} theme - the current theme
 */
function updateThemeSelector (theme) {
  const $html = document.querySelector('html')

  $html.querySelectorAll(`[data-theme=${theme}]`)
    .forEach($el => $el.removeAttribute('onclick'))

  Array.from($html.querySelectorAll(`[data-theme]`))
    .filter($el => $el.getAttribute('data-theme') !== theme)
    .forEach($el => $el.setAttribute(
      'onclick',
      `window.setLocalTheme('${$el.getAttribute('data-theme')}')`
    ))
}

/**
 * updates the injected object with the current theme
 */
function updateInjectedObjects () {
  if (window.applyCurrentTheme) {
    Array.from(document.querySelectorAll('object'))
      .filter($obj => !!$obj.contentDocument)
      .forEach($obj => window.applyCurrentTheme($obj.contentDocument))
  }
}

/**
 * switches the page to the selected theme
 * @param {string} theme - the selected theme
 */
function switchTheme (theme) {
  if (!supportedThemes.has(theme)) return

  const $html = document.querySelector('html')
  $html.setAttribute('data-theme', theme)

  updateThemeSelector(theme)
  updateInjectedObjects()
}

window.switchTheme = switchTheme
window.setLocalTheme = setLocalTheme

switchTheme(getCurrentTheme())
