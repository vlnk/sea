/**
 * all supported language
 * @readonly
 * @enum {string}
 */
const supportedLang = Object.freeze({
  EN: 'en',
  FR: 'fr'
})

/**
 * the default language to use
 * @const {string} defaultLang
 */
const defaultLang = supportedLang.EN

/**
 * all translated strings hashed by unique keys for all supported language
 * @const {object} locales
 */
const locales = {
  [supportedLang.EN]: {
    'about-title': 'About',
    'about-content': 'I am a <strong>full-stack consultant</strong> specialized in web applications made with <strong>ReactJS</strong>, based in <strong>Montreal</strong>. Since I obtained my master of computer science at the UQAC, I engineered, architected, implemented and maintained multiple web applications and I want to share <strong>these 8 years of experience</strong> for concrete needs.',
    'about-platforms': 'Follow me on networks:',

    'manifesto-title': 'Manifesto',
    'manifesto-intro': 'I focus my work on some guidelines:',
    'manifesto-item0': 'Making accessible web services for communities.',
    'manifesto-item1': 'Providing sustainable and maintainable platforms.',
    'manifesto-item2': 'Encouraging the autonomy of my partners with formations.',
    'manifesto-item3': 'Privileging empathy over rentability.',
    'manifesto-item4': 'Constantly reducing the ecological fingerprint of my services.',
    'about-contact': 'Get in touch',

    'portfolio-title': 'Portfolio',

    'bibliolab-title': 'Bibliolab',
    'bibliolab-description': 'When I start my front-end development work at the SAT, I analyze and shape an <strong>ReactJS interface</strong> in order to experiment the use of telepresence systems in public libraries.',
    'bibliolab-references': 'Read <a href="https://www.ledevoir.com/culture/539824/mot-cle-bibliolab-en-co-presence-de-l-artiste">Le Devoir&rsquo;s article</a> and <a href="https://vimeo.com/310599343?embedded=true&source=video_title&owner=885308">see the system in action</a>.',

    'telharmonium-title': 'Telharmonium Web',
    'telharmonium-description': 'I worked with the collective <a href="http://menardstamand.com">Ménard St-Amand</a> in order to improve and extend their web-application <a href="http://menardstamand.com/#/applications/telharmonium">Telharmonium Web</a> made with <strong>Vue.js</strong>. This application recreates their sound installations <em>Telharmonium</em> and <em>On/Off</em>.',

    'scenic-title': 'Téléprésence Scenic',
    'scenic-description1': 'I lead the development of the UI rewriting with <strong>ReactJS</strong> of the telepresence interface named Scenic. It is a key part of the open-sourced and  innovative tools I developed and maintained since 2018 at the <a href="https://sat.qc.ca/">Society of art and technologies (SAT)</a>, a non-profit artistic entity based in Montreal.',
    'scenic-description2': 'I also worked with the <strong>C++</strong> and <strong>Python</strong> languages at the <em>Valorisation of the research</em> department to build systems that controls low-latency transmissions of high-quality audiovisual streams. We help the production of distributed shows such as <a href="https://montreal.ca/evenements/bluff-34464">Bluff</a> that was simultaneously played in three places in the same time.',
    'scenic-description3': 'Now I endorse the role of <strong>Product Owner</strong> and by using the AGILE strategies, we plan the future of Scenic, and we engineer a new artistic medium: <a href="https://sat.qc.ca/en/haptic-floor">the haptic floor</a>. It brings the power of the haptic technologies for the new kind of artistic performances.',

    'theme-dark': 'dark',
    'theme-light': 'light'
  },
  [supportedLang.FR]: {
    'about-title': 'À propos',
    'about-content': 'Je suis un <strong>consultant en conception d&rsquo;applications</strong> spécialisé dans les applications web faites avec <strong>ReactJS</strong> à <strong>Montréal</strong>. Depuis que j&rsquo;ai obtenu ma maitrise en science de l&rsquo;informatique à l&rsquo;UQAC, j&rsquo;ai conçu, planifié, implémenté et maintenu diverses applications web et je veux partager <strong>cette expertise de 8 ans</strong> pour des besoins concrets.',
    'about-platforms': 'Suivez-moi sur les réseaux:',

    'manifesto-title': 'Manifeste',
    'manifesto-intro': 'Je porte une attention particulière sur ces sujets:',
    'manifesto-item0': 'Créer des plateformes web accessibles pour toutes les communautés.',
    'manifesto-item1': 'Livrer des applications robustes et maintenables.',
    'manifesto-item2': 'Encourager l\'autonomie de mes partenaires grâce à des formations.',
    'manifesto-item3': 'Privilégier l\'empathie au détriment de la rentabilité.',
    'manifesto-item4': 'Constamment réduire l\'empreinte écologique de mes services.',
    'about-contact': 'Contactez-moi',

    'portfolio-title': 'Portfolio',

    'bibliolab-title': 'Bibliolab',
    'bibliolab-description': 'Quand j&rsquo;ai commencé mes travaux en tant que front-end développeur à la SAT, j&rsquo;ai analysé et réalisé une <strong>interface en ReactJS</strong> dans le but de créer une expérience de téléprésence dans les bibliothèques publiques.',
    'bibliolab-references': 'Lisez <a href="https://www.ledevoir.com/culture/539824/mot-cle-bibliolab-en-co-presence-de-l-artiste">l&rsquo;article paru sur le journal Le Devoir</a> et <a href="https://vimeo.com/310599343?embedded=true&source=video_title&owner=885308">voyez le système en action</a>.',

    'telharmonium-title': 'Telharmonium Web',
    'telharmonium-description': 'J&rsquo;ai travaillé avec le collectif <a href="http://menardstamand.com">Ménard St-Amand</a> pour améliorer leur application web <a href="http://menardstamand.com/#/applications/telharmonium">Telharmonium Web</a> faite avec <strong>Vue.js</strong>. Cette application recrée leur installation sonore <em>Telharmonium</em> et <em>On/Off</em>.',

    'scenic-title': 'Téléprésence Scenic',
    'scenic-description1': 'J&rsquo;ai mené la réécriture en <strong>ReactJS</strong> de l&rsquo;interface web de téléprésence Scenic. C&rsquo;est un logiciel en source ouverte et innovant que je développe et maintiens depuis 2018 à la <a href="https://sat.qc.ca/">Société des Arts Technologiques (SAT)</a>, une organisation artistique à but non lucratif de Montréal.',
    'scenic-description2': 'J&rsquo;ai aussi travaillé avec les langages <strong>C++</strong> et <strong>Python</strong> au département de la <em>Valorisation de la recherche</em> afin de concevoir des systèmes qui contrôlent la transmission en basse latence de flux audiovisuels de haute qualité. Nous aidons à la production de spectacles distribués tels que <a href="https://montreal.ca/evenements/bluff-34464">Bluff</a> qui s&rsquo;est joué dans trois salles simultanément.',
    'scenic-description3': 'Maintenant, je suis <strong>Responsable des produits</strong> et à l&rsquo;aide des pratiques <strong>AGILE</strong>, nous préparons le futur de Scenic et nous façonnons un nouveau médium artistique: <a href="https://sat.qc.ca/en/haptic-floor">le plancher haptique</a>. Cet outil apporte la puissance des technologies haptiques pour de nouvelles performances artistiques.',

    'theme-dark': 'sombre',
    'theme-light': 'clair'
  }
}

/**
 * compares the user language against the supported languages
 * @param {string} lang - the user langage
 * @returns {string} the supported language that has the better fit
 */
function guessLang (lang) {
  let guessed = undefined

  for (const supported of Object.values(supportedLang)) {
    if (lang.includes(supported)) {
      guessed = supported
      break
    }
  }

  return guessed
}

/**
 * gets the current language to use
 * @returns {string} the supported language that fits better to the user or the default one
 */
function getCurrentLang () {
  const navigatorLang = navigator.language || navigator.userLanguage
  const localLang = localStorage.getItem('lang')

  let currentLang

  if (localLang) {
    currentLang = localLang
  } else if (navigatorLang) {
    currentLang = guessLang(navigatorLang)
  }

  return currentLang || defaultLang
}

/**
 * gets the language parametrized in the URL
 * @returns {string} the supported language that fits better to the user or null
 */
function getUrlLang () {
  const urlLang = new URLSearchParams(window.location.search).get('lang')
  let currentLang

  if (urlLang) {
    currentLang = guessLang(urlLang) || defaultLang
  }

  return currentLang
}

/**
 * sets the selected language in the localStorage and translate the page
 * @param {string} lang - the current supported language
 */
function setLocalLang (newLang) {
  if (newLang === getCurrentLang()) return

  console.debug(`set new local lang ${newLang}`)
  localStorage.setItem('lang', newLang)
  clearTranslations(document.querySelector('html'))
  translateAll(getCurrentLang())
}

/**
 * translates an element with the current supported language
 * @param {HTMLElement} $el - HTML element to translate
 * @param {string} lang - the current supported language
 * @returns {HTMLElement} the translated element
 */
function translate ($el, lang) {
  const locale = locales[lang]
  const key = $el.getAttribute('data-l10n')

  if (locale[key]) {
    $el.innerHTML = locale[key]
    $el.setAttribute('translated', '')
  }

  console.debug(`translated key ${key}`)

  return $el
}

function clearTranslations ($root) {
  Array.from($root.querySelectorAll('[data-l10n]'))
    .forEach($el => $el.removeAttribute('translated'))
}

/**
 * translates all the elements of a page
 * @param {string} lang - the current supported language
 */
function translateAll (lang) {
  const $html = document.querySelector('html')

  $html.setAttribute('lang', lang)

  Array.from($html.querySelectorAll('[data-l10n]'))
    .filter($el => !$el.hasAttribute('translated'))
    .forEach($el => translate($el, lang))

  updateLangSelector(lang)
}

/**
 * updates the lang selectors by binding `onclick` methods
 * @param {string} lang - the current lang
 */
function updateLangSelector (lang) {
  const $html = document.querySelector('html')

  $html.querySelectorAll(`[hreflang=${lang}]`)
    .forEach($el => $el.removeAttribute('onclick'))

  Array.from($html.querySelectorAll(`[hreflang]`))
    .filter($el => $el.getAttribute('hreflang') !== lang)
    .forEach($el => $el.setAttribute(
      'onclick',
      `window.setLocalLang('${$el.getAttribute('hreflang')}')`
    ))
}

/**
 * function called when the module is loaded
 */
function setupLocaleModule () {
  const urlLang = getUrlLang()

  if (urlLang) {
    setLocalLang(urlLang)
  }

  console.info(`current language is ${getCurrentLang()}`)

  const $body = document.querySelector('body')
  const l10nObserver = new MutationObserver(() => translateAll(getCurrentLang()))

  l10nObserver.observe($body, {
    childList: true,
    subtree: true
  })
}

setupLocaleModule()

window.setLocalLang = setLocalLang
