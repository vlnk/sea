async function injectTemplate ($el) {
    const templatePath = $el.getAttribute('template')
    const title = $el.getAttribute('title')
    let mailto = $el.getAttribute('href')

    try {
        const response = await fetch(templatePath)
        const template = encodeURI(await response.text())

        if (response.ok) {
            $el.setAttribute('href', `${mailto}?subject=${title}&body=${template}`)
            $el.removeAttribute('template')
            $el.removeAttribute('title')
        }
    } catch (err) {
        console.error(err.message)
    }

    return $el
}

function injectAll () {
    document.querySelectorAll('a[template]')
        .forEach($el => injectTemplate($el))
}

injectAll()
