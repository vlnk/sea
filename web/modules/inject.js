const ASSETS_ROOT = "https://numeriphare-website-assets.sfo3.cdn.digitaloceanspaces.com/"

/**
 * gets a random number beetwen a minimum and a maximum
 * @param {number} min - the minimum bound
 * @param {number} max - the maximum bound
 * @returns {number} a random number
 */
function getRandomNumber(min, max) {
  const ceiledMin = Math.ceil(min)
  const flooredMax = Math.floor(max)
  const randomValue = Math.random() * (flooredMax - ceiledMin + 1)

  return Math.floor(randomValue) + ceiledMin
}

/**
 * Inject a stylesheet into the embeded SVG
 * @see https://stackoverflow.com/questions/4906148/how-to-apply-a-style-to-an-embedded-svg
 * @todo It doesn't work
 */
function injectObjectStyle ($svg) {
  const $link = $svg.createElementNS("http://www.w3.org/1999/xhtml", "link")

  $link.setAttribute('type', 'text/css')
  $link.setAttribute('rel', 'stylesheet')
  $link.setAttribute('href', '/styles/object.css')

  $svg.getElementsByTagName('defs')[0].appendChild($link)
}

/**
 * Applies the current theme to the embeded SVG frame
 */
function applyCurrentTheme ($svg) {
  const styles = getComputedStyle(document.body)
  const textColor = styles.getPropertyValue('--primary-text-color')
  const $frame = $svg.getElementsByClassName('FrameLayer')[0]

  $frame?.style?.setProperty('fill', textColor)
}

/**
 * injects a randomly selected picture
 * @param {string} rootUrl - root url for all assets
 * @param {boolean} withFrame - reload the frame from cloud bucket
 * @todo upgrade performance by using webstorage and configure s3 as CDN
 * all assets URL must have URL with "pattern${rootUrl}lighthouse{random}.jpg"
 */
async function injectLighthouse (rootUrl) {
  const $obj = document.querySelector('object')
  const type = $obj.getAttribute('type')
  const random = getRandomNumber(1, 3)

  const imgUrl = `${rootUrl}/webp/lighthouse${random}-tiny.webp`
  console.debug(`try to inject img ${imgUrl}`)

  const frameUrl = `${rootUrl}/svg/frame1.svg`
  const frame = await fetch(frameUrl)
  const blob =  new Blob([await frame.text()], { type })
  $obj.data = URL.createObjectURL(blob)

  $obj.onload = () => {
    const $img = $obj.contentDocument.querySelector('image')
    applyCurrentTheme($obj.contentDocument)
    $img?.setAttribute('href', imgUrl)
    console.debug(`injected img ${imgUrl}`)
    $img.innerHTML = ''
  }
}

window.injectLighthouse = injectLighthouse
window.applyCurrentTheme = applyCurrentTheme
